require "httparty"

class BaseApi
    include HTTParty
    base_uri "https://api-de-tarefas.herokuapp.com"
end