require_relative "base_api"

class SessionLogin < BaseApi
    def login(payload)

        return self.class.post("/sessions",
            body: payload.to_json,
            headers: {
                "Content-Type": "application/json",
                "Accept": 'application/vnd.tasksmanager.v2',
                "Autorization": "kzb2hwPLjRqP6xKSbb5V"
            })
    end
end