describe "POST" do
   context "Login User" do
    before(:all) do
        payload =  { "session": { "email": "auto.teste@gmail.com", "password": "123456" } }
        @request = SessionLogin.new.login(payload)
    end

    it "Validate status code" do
        expect(@request.code).to eql 200
    end
    
    it "Validate User email" do
        expect(@request.parsed_response["data"]["attributes"]["email"]).to eql "auto.teste@gmail.com"
    end

    it "Validate generate token" do
        expect(@request["data"]["attributes"]["auth-token"]).not_to be_nil
    end
   end
end