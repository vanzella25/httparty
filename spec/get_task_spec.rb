describe "GET" do
    context "Tasks" do
     before(:all) do
        payload =  { "session": { "email": "automacao@qa.com", "password": "123456" } }
        login_user = SessionLogin.new.login(payload)

        @token = login_user["data"]["attributes"]["auth-token"]
        @request = Tasks.new.list_task(@token)
     end
         
     it "Validate status code" do
         expect(@request.code).to eql 200
     end
     
     it "Validate body not null" do
         expect(@request.body).not_to be_nil
     end
    end
end